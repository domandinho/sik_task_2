#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <inttypes.h>
#include <endian.h>
#include <net/if.h>

#include "mdnsapi.h"
#include "config.h"
#include "err.h"

void m_dns_receive(struct UserConfiguration configuration) {
    socklen_t lengthOfSocketAddress = sizeof(struct sockaddr);
    int identificatorOfSocket = configuration.UserInterfacePort;
    //used because this variable is unused in this module
    struct sockaddr addr;
    int i = 0;
    int flags = 0;
    struct sockaddr MDNSsocketAddress;
    ((struct sockaddr_in *) &MDNSsocketAddress)->sin_family = AF_INET;
    ((struct sockaddr_in *) &MDNSsocketAddress)->sin_addr.s_addr = inet_addr("224.0.0.251");
    ((struct sockaddr_in *) &MDNSsocketAddress)->sin_port = htons(5353);
    socklen_t lengthOfSendedMessage;
    ssize_t lengthOfReceivedMessage;
    unsigned char bufferReceivedAnswer[MAX_BUF_SIZE];
    unsigned char bufferSendedMessage[MAX_BUF_SIZE];
    struct DNS_HEADER *dnsHeader = NULL;
    struct RES_RECORD *responseRecord;
    struct QUERY *queryRecord;

    while (1) {
        lengthOfReceivedMessage = recvfrom(identificatorOfSocket, &bufferReceivedAnswer, sizeof(bufferReceivedAnswer),
                                           flags, &addr,
                                           &lengthOfSocketAddress);
        if (lengthOfReceivedMessage < 0) {
            syserr("error on datagram from client socket");
        } else {
            dnsHeader = (struct DNS_HEADER *) bufferReceivedAnswer;
            int numberOfQuestions = ntohs(dnsHeader->q_count);
            if (numberOfQuestions > 0) {
                queryRecord = MDNSApigetQuestion(bufferReceivedAnswer);
                for (i = 0; i < numberOfQuestions; i++) {
                    if (MDNSApiCreateAnswer(queryRecord[i].name, ntohs(queryRecord[i].ques->qtype), bufferSendedMessage,
                                            &lengthOfReceivedMessage)) {
                        lengthOfSendedMessage = sendto(identificatorOfSocket, bufferSendedMessage,
                                                       lengthOfReceivedMessage, flags,
                                                       &MDNSsocketAddress, lengthOfSocketAddress);
                        if (lengthOfSendedMessage != lengthOfReceivedMessage)
                            syserr("error on sending datagram to client socket");
                    }
                }
                free(queryRecord);
            } else if (ntohs(dnsHeader->ans_count)) { // we've got answer over here!
                responseRecord = MDNSApigetAnswer(bufferReceivedAnswer);
                for (i = 0; i < ntohs(dnsHeader->ans_count); i++) {
                    int result = MDNSApiCreateQuestion(responseRecord[i].name, responseRecord[i].rdata,
                                                       ntohs(responseRecord[i].resource->type), bufferSendedMessage,
                                                       &lengthOfReceivedMessage, &configuration);
                    if (result) {
                        lengthOfSendedMessage = sendto(identificatorOfSocket, bufferSendedMessage,
                                                       lengthOfReceivedMessage, flags,
                                                       &MDNSsocketAddress, lengthOfSocketAddress);
                        if (lengthOfSendedMessage != lengthOfReceivedMessage)
                            syserr("error on sending datagram to client socket");
                    }
                }
                free(responseRecord);
            }
        }
    }
}

void *threadServiceDNSReceiver(void *baseConfiguration) {
    struct UserConfiguration *pointerToDataWhichWillBeFreedNow = (struct UserConfiguration *) baseConfiguration;
    struct UserConfiguration configuration = *pointerToDataWhichWillBeFreedNow;
    free(pointerToDataWhichWillBeFreedNow);
    m_dns_receive(configuration);
    return NULL;
}

void runServiceDNSReceiver(struct UserConfiguration *baseConfiguration,
                           int socketNumber) {
    pthread_t t;
    struct UserConfiguration *configuration = malloc(sizeof(struct UserConfiguration));
    if (!configuration) {
        syserr("malloc");
    }
    *configuration = *baseConfiguration;
    configuration->UserInterfacePort = socketNumber;
    int receiveAnswerCode = pthread_create(&t, 0, threadServiceDNSReceiver,
                                           (void *) configuration);
    if (receiveAnswerCode == -1) {
        syserr("pthread_create");
    }
    receiveAnswerCode = pthread_detach(t);
    if (receiveAnswerCode == -1) {
        syserr("pthread_detach");
    }
}