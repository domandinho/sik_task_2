#include "countmicrosecondsservice.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <inttypes.h>

uint64_t countMicroseconds() {
	struct timeval actualTime;
	struct timezone timeZone;
	struct tm * timePointer;
	gettimeofday(&actualTime, &timeZone);
	timePointer = localtime(&actualTime.tv_sec);
	uint64_t seconds = (uint64_t) mktime(timePointer);
	uint64_t microseconds = seconds * 1000000 + actualTime.tv_usec;
	return microseconds;
}
