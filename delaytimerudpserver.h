/*
 * delaytimerudpserver.h
 *
 *  Created on: 22 maj 2015
 *      Author: domandinho
 */

#ifndef DELAYTIMERUDPSERVER_H_
#define DELAYTIMERUDPSERVER_H_

#include <inttypes.h>
#include "config.h"

void runServiceUDPServer(struct UserConfiguration *baseConfiguration);

#endif /* DELAYTIMERUDPSERVER_H_ */
