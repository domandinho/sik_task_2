#ifndef CONFIG_H
#define    CONFIG_H

#ifdef    __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <inttypes.h>
#include <pthread.h>
#include "listimplementation.h"

struct UserConfiguration {
    int measureDelayTimeIntervals;
    int measureDetectingServersInterval;
    int ICMPPackageHeaderId;
    int32_t ICMPPackageData;
    int UDPMeasureDelayTimeServerPort;
    int UserInterfacePort;
    bool DNSdelayMode;
    double UserInterfaceRefreshInterval;
    pthread_mutex_t *DelayCounterHostListMutex;
    pthread_mutex_t *MDNSServerHostListMutex;
    struct DelayStructuresList *DelayCounterHostList;
    struct DelayStructuresList *MDNSServerHostList;
    char DelayCounterHostname[15];
    int DelayCounterPort;
};

void sleepSeconds(int howManySeconds);

void setDefaultConfigurationValues(struct UserConfiguration *configuration);

void setConfigurationValuesFromInput(struct UserConfiguration *configuration,
                                     int argc, char **argv);

void printConfiguration(struct UserConfiguration *configuration);

void initConfiguration(struct UserConfiguration *configuration, int argc,
                       char **argv);

#ifdef    __cplusplus
}
#endif

#endif	/* CONFIG_H */

