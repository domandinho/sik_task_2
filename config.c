#include "config.h"
#include "err.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <sys/types.h>
#include <stdbool.h>
#include <unistd.h>

static void initMutex(pthread_mutex_t *mutex) {
    int errorCode = pthread_mutex_init(mutex, 0);
    if (errorCode != 0) {
        syserr("mutex init failed");
    }
}

void setDefaultConfigurationValues(struct UserConfiguration *configuration) {
    configuration->measureDelayTimeIntervals = 1;
    configuration->measureDetectingServersInterval = 10;
    configuration->UDPMeasureDelayTimeServerPort = 3382;
    configuration->UserInterfacePort = 3637;
    configuration->DNSdelayMode = false;
    configuration->UserInterfaceRefreshInterval = 1;
    configuration->ICMPPackageHeaderId = 0x13;
    configuration->ICMPPackageData = 879296770;
    initMutex(configuration->DelayCounterHostListMutex);
    initMutex(configuration->MDNSServerHostListMutex);
}

static void retrieveValueFromInput(char *optionToken,
                                   int *configurationVariable, int argc, char **argv) {
    int i;
    for (i = 0; i < argc; i++) {
        if (strcmp(argv[i], optionToken) == 0) {
            if (i + 1 == argc) {
                syserr("You should pass integer value as next argument!");
            } else {
                *configurationVariable = atoi(argv[i + 1]);
            }
        }
    }
}

static void retrieveRefreshIntervalFromInput(
        struct UserConfiguration *configuration, int argc, char **argv) {
    int i;
    for (i = 0; i < argc; i++) {
        if (strcmp(argv[i], "-v") == 0) {
            if (i + 1 == argc) {
                syserr("You should pass integer value as next argument!");
            } else {
                configuration->UserInterfaceRefreshInterval = atof(argv[i + 1]);
            }
        }
    }
}

static void retrieveDelayModeFromInput(struct UserConfiguration *configuration,
                                       int argc, char **argv) {
    int i;
    for (i = 0; i < argc; i++) {
        if (strcmp(argv[i], "-s") == 0) {
            configuration->DNSdelayMode = true;
        }
    }
}

void setConfigurationValuesFromInput(struct UserConfiguration *configuration,
                                     int argc, char **argv) {
    retrieveValueFromInput("-t", &(configuration->measureDelayTimeIntervals),
                           argc, argv);
    retrieveValueFromInput("-u",
                           &(configuration->UDPMeasureDelayTimeServerPort), argc, argv);
    retrieveValueFromInput("-U", &(configuration->UserInterfacePort), argc,
                           argv);
    retrieveValueFromInput("-T",
                           &(configuration->measureDetectingServersInterval), argc, argv);
    retrieveRefreshIntervalFromInput(configuration, argc, argv);
    retrieveDelayModeFromInput(configuration, argc, argv);
}

void initConfiguration(struct UserConfiguration *configuration, int argc,
                       char **argv) {
    setDefaultConfigurationValues(configuration);
    setConfigurationValuesFromInput(configuration, argc, argv);
}

void sleepSeconds(int howManySeconds) {
    struct timeval tv;
    tv.tv_sec = howManySeconds;
    tv.tv_usec = 0;
    select(0, NULL, NULL, NULL, &tv);
}
