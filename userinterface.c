#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "userinterface.h"
#include "listimplementation.h" 
#include "err.h"
#include "delaycounterhostlistapi.h"

#define IAC     "\377"
#define WILL    "\373"
#define SGA     "\003"
#define ECHO    "\001"

void setGetCharMode(char *bafer, int connfd) {
    write(connfd, IAC, 1);
    write(connfd, WILL, 1);
    write(connfd, SGA, 1);
    recv(connfd, bafer, 3, MSG_WAITALL);
    write(connfd, IAC, 1);
    write(connfd, WILL, 1);
    write(connfd, ECHO, 1);
    recv(connfd, bafer, 3, MSG_WAITALL);
}

void *handle_connection(void *pointerToOriginalConfiguration) {
    int returnedCode;
    socklen_t lengthOfSocket;
    struct sockaddr_in addr;
    struct UserConfiguration *configurationToFree =
            (struct UserConfiguration *) (pointerToOriginalConfiguration);
    struct UserConfiguration configuration = *configurationToFree;
    free(configurationToFree);
    int socketAddress = configuration.ICMPPackageHeaderId;
    lengthOfSocket = sizeof(addr);
    returnedCode = getpeername(socketAddress, (struct sockaddr *) &addr, &lengthOfSocket);
    if (returnedCode == -1) {
        perror("getsockname");
        exit(1);
    }
    char inputFromTelnetClient[4096];
    int number = 0;
    setGetCharMode(inputFromTelnetClient, socketAddress);
    char bufforOfClearingScreen[100];
    sprintf(bufforOfClearingScreen, "%c[2J %c[H",
            (char) 27, (char) 27);
    char text[26 * 80];
    int firstElementIndex = 0;
    int k = 0;
    for (; ;) {
        for (k = 0; k < 26 * 80; k++) {
            text[k] = (char) (0);
        }
        int didUserPressKey = recv(socketAddress, inputFromTelnetClient, 1, MSG_WAITALL);
        if (didUserPressKey < 0) {
            inputFromTelnetClient[0] = ' ';
        } else {
            if (inputFromTelnetClient[0] == 't') {
                break;
            }
            if (inputFromTelnetClient[0] == 'q' && firstElementIndex != 0) {
                firstElementIndex--;
            } else {
                if (inputFromTelnetClient[0] == 'a' && firstElementIndex !=
                                                               configuration.DelayCounterHostList->size - 1) {
                    firstElementIndex++;
                }
            }
        }
        UserInterfacePrintQueue(&configuration, text, &firstElementIndex);
        write(socketAddress, bufforOfClearingScreen, strlen(bufforOfClearingScreen));
        write(socketAddress, text, strlen(text));
        number++;
    }
    close(socketAddress);
    return 0;
}

void serviceUserInterface(struct UserConfiguration configuration) {
    int TCPIdOfSocket, receiveAnswerCode;
    socklen_t lengthOfSocket;
    struct sockaddr_in server;
    /* Tworzymy gniazdko */
    TCPIdOfSocket = socket(PF_INET, SOCK_STREAM, 0);
    struct timeval timeout = {configuration.UserInterfaceRefreshInterval, 0};
    setsockopt(TCPIdOfSocket, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout, sizeof(struct timeval));
    if (TCPIdOfSocket == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }
    /* Podłączamy do centrali */
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(configuration.UserInterfacePort);
    receiveAnswerCode = bind(TCPIdOfSocket, (struct sockaddr *) &server, sizeof(server));
    if (receiveAnswerCode == -1) {
        perror("bind");
        exit(1);
    }
    /* Każdy chce wiedzieć jaki to port */
    lengthOfSocket = (socklen_t) sizeof(server);
    receiveAnswerCode = getsockname(TCPIdOfSocket, (struct sockaddr *) &server, &lengthOfSocket);
    if (receiveAnswerCode == -1) {
        perror("getsockname");
        exit(EXIT_FAILURE);
    }
    receiveAnswerCode = listen(TCPIdOfSocket, 5);
    if (receiveAnswerCode == -1) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    /* No i do pracy */
    for (; ;) {
        int telnetClientSocketId = 0;
        telnetClientSocketId = accept(TCPIdOfSocket, (struct sockaddr *) NULL, NULL);
        if (telnetClientSocketId == -1) {
            continue;
        }
        struct UserConfiguration *configurationToPass;
        /* Tylko dla tego wątku */
        configurationToPass = malloc(sizeof(struct UserConfiguration));
        if (!configurationToPass) {
            syserr("malloc");
            exit(EXIT_FAILURE);
        }
        pthread_t t;
        *configurationToPass = configuration;
        configurationToPass->ICMPPackageHeaderId = telnetClientSocketId;
        receiveAnswerCode = pthread_create(&t, 0, handle_connection, configurationToPass);
        if (receiveAnswerCode == -1) {
            perror("pthread_create");
            exit(EXIT_FAILURE);
        }
        /* No przecież nie będę na niego czekał ... */
        receiveAnswerCode = pthread_detach(t);
        if (receiveAnswerCode == -1) {
            perror("pthread_detach");
            exit(EXIT_FAILURE);
        }
    }
}

void *threadUserInterface(void *baseConfiguration) {
    struct UserConfiguration *pointerToDataWhichWillBeFreedNow = (struct UserConfiguration *) baseConfiguration;
    struct UserConfiguration configuration = *pointerToDataWhichWillBeFreedNow;
    free(pointerToDataWhichWillBeFreedNow);
    serviceUserInterface(configuration);
    return NULL;
}

void runUserInterfaceModule(struct UserConfiguration *baseConfiguration) {
    pthread_t t;
    struct UserConfiguration *configuration = malloc(sizeof(struct UserConfiguration));
    if (!configuration) {
        syserr("malloc");
    }
    *configuration = *baseConfiguration;
    int receiveAnswerCode = receiveAnswerCode = pthread_create(&t, 0, threadUserInterface, configuration);
    if (receiveAnswerCode == -1) {
        syserr("pthread_create");
    }
    receiveAnswerCode = pthread_detach(t);
    if (receiveAnswerCode == -1) {
        syserr("pthread_detach");
    }
}

/*EOF*/
