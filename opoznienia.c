#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <sys/types.h>
#include <stdbool.h>
#include "err.h"
#include "config.h"
#include "delaytimerudpserver.h"
#include "delaytimerudp.h"
#include "delaytimertcp.h"
#include "delaytimericmp.h"
#include "delaycounter.h"
#include "mdnsserver.h"
#include <pthread.h>
#include <unistd.h>
#include "delaycounterhostlistapi.h"
#include "userinterface.h"
#include "delaycounter.h"

void run(int argc, char **argv) {
    pthread_mutex_t DelayCounterHostListMutex;
    pthread_mutex_t MDNSServerHostListMutex;
    struct DelayStructuresList DelayCounterHostList;
    struct DelayStructuresList MDNSServerHostList;
    struct UserConfiguration configuration;
    int errorCode = 0;
    DelayCounterHostList.size = 0;
    MDNSServerHostList.size = 0;
    errorCode = pthread_mutex_init(&DelayCounterHostListMutex, NULL);
    if (errorCode != 0) {
        syserr("pthread mutex init");
    }
    errorCode = pthread_mutex_init(&MDNSServerHostListMutex, NULL);
    if (errorCode != 0) {
        syserr("pthread mutex init");
    }
    configuration.DelayCounterHostList = &DelayCounterHostList;
    configuration.MDNSServerHostList = &MDNSServerHostList;
    configuration.MDNSServerHostListMutex = &MDNSServerHostListMutex;
    configuration.DelayCounterHostListMutex = &DelayCounterHostListMutex;
    initConfiguration(&configuration, argc, argv);
    runUserInterfaceModule(&configuration);
    runServiceUDPServer(&configuration);
    runThreadRunner(&configuration);
}

int main(int argc, char **argv) {
    run(argc, argv);
    return (EXIT_SUCCESS);
}

