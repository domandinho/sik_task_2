#include "listimplementation.h"
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include "string.h"

struct DelayStructure *create_list(struct DelayStructuresList *list) {
    list->head = NULL;
    list->curr = NULL;
    struct DelayStructure *ptr = (struct DelayStructure *) malloc(sizeof(struct DelayStructure));
    if (NULL == ptr) {
        syserr("malloc");
        return NULL;
    }
    ptr->next = NULL;
    list->head = list->curr = ptr;
    return ptr;
}

struct DelayStructure *add_to_list(bool add_to_end, struct DelayStructuresList *list) {
    list->size = list->size + 1;
    if (NULL == list->head) {
        return (create_list(list));
    }
    struct DelayStructure *ptr = (struct DelayStructure *) malloc(sizeof(struct DelayStructure));
    if (NULL == ptr) {
        syserr("malloc");
    }
    ptr->next = NULL;
    if (add_to_end) {
        list->curr->next = ptr;
        list->curr = ptr;
    }
    else {
        ptr->next = list->head;
        list->head = ptr;
    }
    return ptr;
}

struct DelayStructure *search_in_list(char *hostname, struct DelayStructure **prev, struct DelayStructuresList *list) {
    struct DelayStructure *ptr = list->head;
    struct DelayStructure *tmp = NULL;
    bool found = false;
    int i = 0;
    for (i = 0; i < list->size; i++) {
        if (strcmp(ptr->hostname, hostname) == 0) {
            found = true;
            break;
        }
        else {
            tmp = ptr;
            ptr = ptr->next;
        }
    }
    if (true == found) {
        if (prev)
            *prev = tmp;
        return ptr;
    }
    else {
        return NULL;
    }
}

int delete_from_list(char *hostname, struct DelayStructuresList *list) {
    struct DelayStructure *prev = NULL;
    struct DelayStructure *del = NULL;
    del = search_in_list(hostname, &prev, list);
    if (del == NULL) {
        return -1;
    }
    else {
        list->size--;
        if (prev != NULL)
            prev->next = del->next;
        if (del == list->curr) {
            list->curr = prev;
        }
        else if (del == list->head) {
            list->head = del->next;
        }
    }
    free(del);
    del = NULL;
    return 0;
}

int delayAverage(int *table) {
    int i = 0, numberOfNonZeroResults = 0;
    double result = 0;
    for (i = 0; i < 10; i++) {
        if (table[i] != 0) {
            numberOfNonZeroResults++;
        }
        result += table[i];
    }
    if (numberOfNonZeroResults == 0) {
        result = 0;
    } else {
        result /= numberOfNonZeroResults;
    }
    return result;
}

void printDouble(int *j, char *text, int value) {
    char buffer[30];
    int numberOfSeconds = value / 1000000;
    int k = 0;
    if (numberOfSeconds >= 10) {
        //timeout case
        text[*j] = '1';
        (*j)++;
        text[*j] = '0';
        (*j)++;
        text[*j] = '.';
        (*j)++;
        text[*j] = '0';
        (*j)++;
        text[*j] = '0';
        (*j)++;
        return;
    } else {
        value %= 1000000; //parts of second
        value += 1000000; //because we want 001 not 1
        sprintf(buffer, "%d%d", numberOfSeconds, value);
        for (k = 0; k < 8; k++) {
            text[*j] = buffer[k];
            if (k == 1) {
                text[*j] = '.';
            }
            (*j)++;
        }
    }
}

void insertSpace(int *j, char *text) {
    text[*j] = ' ';
    (*j)++;
}

int minValue(int a, int b) {
    if (a < b) {
        return a;
    } else {
        return b;
    }
}

void print_list(struct DelayStructuresList *list, char *text, int *begin) {
    bubbleSort(list);
    if (*begin > list->size) {
        *begin = list->size;
    }
    int i = 0, j = 0;
    int numberOfRecords = 23;
    struct DelayStructure *actual = list->head;
    while (i < *begin) {
        actual = actual->next;
        i++;
    }
    for (i = 0; i < minValue(numberOfRecords, list->size - *begin); i++) {
        int k = 0;
        for (k = 0; k < strlen(actual->hostname); k++) {
            text[j] = actual->hostname[k];
            j++;
        }
        double TCPDelayAverage = delayAverage(actual->TCPdelay);
        double UDPDelayAverage = delayAverage(actual->UDPdelay);
        double ICMPDelayAverage = delayAverage(actual->ICMPdelay);
        double totalDelayAverage = (TCPDelayAverage +
                                    UDPDelayAverage + ICMPDelayAverage) / 3000000;
        k = (int) (totalDelayAverage);
        while (k > 0) {
            k--;
            insertSpace(&j, text);
        }
        insertSpace(&j, text);
        printDouble(&j, text, TCPDelayAverage);
        insertSpace(&j, text);
        printDouble(&j, text, UDPDelayAverage);
        insertSpace(&j, text);
        printDouble(&j, text, ICMPDelayAverage);
        text[j] = 10;
        j++;
        text[j] = 13;
        j++;
        actual = actual->next;
    }
}

void popFrontList(struct DelayStructuresList *list) {
    if (list->size > 0) {
        delete_from_list(list->head->hostname, list);
    }
}

void clearList(struct DelayStructuresList *list) {
    while (list->size > 0) {
        popFrontList(list);
    }
}

bool isElementExistsOnList(
        char *hostname, struct DelayStructuresList *list) {
    if (search_in_list(hostname, NULL, list) != NULL) {
        return true;
    } else {
        return false;
    }
}

void showListContent(struct DelayStructuresList *list) {
    struct DelayStructure *iterator = list->head;
    int i = 0;
    for (i = 0; i < list->size; i++) {
        iterator = iterator->next;
    }
}

static double averageFromTables(struct DelayStructure *element) {
    double delay = 0;
    delay += delayAverage(element->TCPdelay);
    delay += delayAverage(element->UDPdelay);
    delay += delayAverage(element->ICMPdelay);
    delay /= 3;
    return delay;
}

static bool compareOperator(struct DelayStructure *first,
                     struct DelayStructure *second) {
    double firstResult = averageFromTables(first);
    double secondResult = averageFromTables(second);
    if (firstResult >= secondResult) {
        return false;
    } else {
        return true;
    }
}

static void copyValuesFromDelayStructure(struct DelayStructure *changed,
                                  struct DelayStructure *fixed) {
    int i = 0;
    for (i = 0; i < 10; i++) {
        changed->TCPdelay[i] = fixed->TCPdelay[i];
        changed->UDPdelay[i] = fixed->UDPdelay[i];
        changed->ICMPdelay[i] = fixed->ICMPdelay[i];
    }
    for (i = 0; i < 15; i++) {
        changed->hostname[i] = fixed->hostname[i];
    }
    for (i = 0; i < 8; i++) {
        changed->port[i] = fixed->port[i];
    }
}

void bubbleSort(struct DelayStructuresList *list) {
    if (list->size == 0) {
        return;
    }
    int i = 0, j = 0;
    for (i = 0; i < list->size; i++) {
        struct DelayStructure *first = list->head;
        struct DelayStructure *second = list->head->next;
        int j = 0;
        for (j = 0; j < list->size - 1; j++) {
            if (compareOperator(first, second) == true) {
                struct DelayStructure buffer;
                copyValuesFromDelayStructure(&buffer, second);
                copyValuesFromDelayStructure(second, first);
                copyValuesFromDelayStructure(first, &buffer);
            }
            first = first->next;
            second = second->next;
        }
    }
}