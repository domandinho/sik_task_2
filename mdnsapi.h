#include "config.h"

#ifndef SIECI_MDNSAPI_H
#define SIECI_MDNSAPI_H
#define T_A 1
#define T_PTR 12
#define MAX_BUF_SIZE 65536

struct DNS_HEADER {
    unsigned short id;
    unsigned char rd :1;
    unsigned char tc :1;
    unsigned char aa :1;
    unsigned char opcode :4;
    unsigned char qr :1;

    unsigned char rcode :4;
    unsigned char cd :1;
    unsigned char ad :1;
    unsigned char z :1;
    unsigned char ra :1;

    unsigned short q_count;
    unsigned short ans_count;
    unsigned short auth_count;
    unsigned short add_count;
};

struct QUESTION {
    unsigned short qtype;
    unsigned short qclass;
};

#pragma pack(push, 1)
struct R_DATA {
    unsigned short type;
    unsigned short _class;
    unsigned int ttl;
    unsigned short data_len;
};
#pragma pack(pop)

struct RES_RECORD {
    unsigned char *name;
    struct R_DATA *resource;
    unsigned char *rdata;
};

//Structure of a Query
struct QUERY {
    unsigned char *name;
    struct QUESTION *ques;
};

void setMyOwnHostName();
void setMyOwnIpAddress();
void MDNSApiParseMessage(char *buffer);
int MDNSApiCreateAnswer(unsigned char *query, int type,
                        unsigned char *buffer, ssize_t *lengthOfMDNSHeader);
int MDNSApiCreateQuestion(unsigned char *name, unsigned char *responseData,
                          int type, unsigned char *buffer, ssize_t *length,
                          struct UserConfiguration *configuration);
struct RES_RECORD *MDNSApigetAnswer(char *buffer);
struct QUERY *MDNSApigetQuestion(char *buffer);
u_char *ReadName(unsigned char *reader,
                 unsigned char *buffer, int *count);
#endif //SIECI_MDNSAPI_H
