#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <inttypes.h>
#include <endian.h>
#include <net/if.h>
#include "mdnsapi.h"
#include "delaycounterhostlistapi.h"

char utilNameOpoznieniaUDP[] = "_opoznienia._udp.local.";
char utilNameSshTCP[] = "_ssh._tcp.local.";

char *stringNameOfHost[2000];
char *stringOpoznieniaUtil[2000];
char *stringSSHTcpUtil[2000];

struct sockaddr_in myOwnIpAddressMDNSapi;

void setMyOwnIpAddress() {
    char nameOfInterface[] = "eth0";
    struct ifreq ipGetter;
    int numberOfOpenedSocket;
    numberOfOpenedSocket = socket(AF_INET, SOCK_DGRAM, 0);
    ipGetter.ifr_addr.sa_family = AF_INET;
    strncpy(ipGetter.ifr_name, nameOfInterface, IFNAMSIZ - 1);
    ioctl(numberOfOpenedSocket, SIOCGIFADDR, &ipGetter);
    close(numberOfOpenedSocket);
    myOwnIpAddressMDNSapi = *((struct sockaddr_in *) &ipGetter.ifr_addr);
}

static void stringOperationsOnHostName(char *host) {
    struct hostent *h;
    h = gethostbyname(host);
    strcpy(stringNameOfHost, h->h_name);
    strcpy(stringOpoznieniaUtil, h->h_name);
    strcpy(stringSSHTcpUtil, h->h_name);
    strcat(stringOpoznieniaUtil, ".");
    strcat(stringSSHTcpUtil, ".");
    strcat(stringOpoznieniaUtil, utilNameOpoznieniaUDP);
    strcat(stringSSHTcpUtil, utilNameSshTCP);
}

void setMyOwnHostName() {
    char host[2000];
    host[1999] = '\0';
    gethostname(host, 1999);
    stringOperationsOnHostName(host);
}

void aton(unsigned char *hostname, unsigned char *networkName) {
    int i = 0;
    size_t lenghtOfHostName = strlen((char *) hostname);
    int lock = 0;
    for (i = 0; i < lenghtOfHostName; i++) {
        if (hostname[i] == '.') {
            *networkName++ = i - lock;
            for (; lock < i; lock++) {
                *networkName++ = hostname[lock];
            }
            lock++;
        }
    }
    *networkName++ = '\0';
}

static void initMDNSHeader(struct DNS_HEADER *dns) {
    dns->id = (unsigned short) htons(0);
    dns->opcode = 0; //This is a standard query
    dns->aa = 0; //Not Authoritative
    dns->tc = 0; //This message is not truncated
    dns->rd = 0; //Recursion Desired
    dns->ra = 0; //Recursion not available! hey we dont have it (lol)
    dns->z = 0;
    dns->ad = 0;
    dns->cd = 0;
    dns->rcode = 0;
    dns->q_count = 0;
    dns->ans_count = 0;
    dns->auth_count = 0;
    dns->add_count = 0;
}

void create_mdns_header(unsigned char *buffer, int type) {
    struct DNS_HEADER *mdnsHeader = NULL;
    mdnsHeader = (struct DNS_HEADER *) buffer;
    initMDNSHeader(mdnsHeader);
    if (type == 0) {
        mdnsHeader->qr = 0;
        mdnsHeader->q_count = htons(1);
    } else {
        if (type == 1) {
            mdnsHeader->qr = 1;
            mdnsHeader->ans_count = htons(1);
        }
    }
}

static void initResource(int type, struct R_DATA *resource) {
    resource->type = htons(type); //type of the query, A or PTR
    resource->_class = htons(1); //its internet
    resource->ttl = 0; // TTL in seconds
}

static int processPTRAnswer(unsigned char *query, ssize_t *lengthOfMDNSHeader, unsigned char *responseData,
                            struct R_DATA *resource) {
    if (!strcmp(query, utilNameOpoznieniaUDP)) {
        aton(stringOpoznieniaUtil, responseData);
    } else {
        if (!strcmp(query, utilNameSshTCP)) {
            aton(stringSSHTcpUtil, responseData);
        } else {
            return 0;
        }
    }
    resource->data_len = htons(strlen((const char *) responseData) + 1); // the lenght of RR specific data in octets
    *lengthOfMDNSHeader += (strlen((const char *) responseData) + 1);
    return 1;
}

static void processAAnswer(ssize_t *lengthOfMDNSHeader, unsigned char *responseData, struct R_DATA *resource) {
    memcpy(responseData, &myOwnIpAddressMDNSapi.sin_addr, 4);  // copy my IP addr
    resource->data_len = htons(4); // size of IP addr
    *lengthOfMDNSHeader += 4;
}

int MDNSApiCreateAnswer(unsigned char *query, int type, unsigned char *buffer,
                  ssize_t *lengthOfMDNSHeader) {
    unsigned char *name, *responseData;
    struct R_DATA *resource = NULL;
    create_mdns_header(buffer, 1);
    name = (unsigned char *) &buffer[sizeof(struct DNS_HEADER)];
    aton(query, name);
    resource = (struct R_DATA *) &buffer[sizeof(struct DNS_HEADER) + (strlen((const char *) name) + 1)]; //fill it
    initResource(type, resource);
    responseData = (unsigned char *) &buffer[sizeof(struct DNS_HEADER) + (strlen((const char *) name) + 1) +
                                             sizeof(struct R_DATA)];
    *lengthOfMDNSHeader = sizeof(struct DNS_HEADER) + sizeof(struct R_DATA) + (strlen((const char *) name) + 1);
    if (type == T_PTR) {
        return processPTRAnswer(query, lengthOfMDNSHeader, responseData, resource);
    } else {
        if (type == T_A) {
            processAAnswer(lengthOfMDNSHeader, responseData, resource);
            return 1;
        } else {
            return 0;
        }
    }
}

void create_or_add(uint32_t ip, char *type,
                   struct UserConfiguration *configuration,
                   bool isItTcpConnection) {
    struct sockaddr_in addressOfHost;
    addressOfHost.sin_family = AF_INET;
    addressOfHost.sin_addr.s_addr = ip;
    char ipaddress[30];
    sprintf(ipaddress, "%s", inet_ntoa(addressOfHost.sin_addr));
//    printf("IP address %s of type %s\n", ipaddress, type);
//    printf("ADDING\n");
    insertRecordToMDNSListOnlyIfItDoesntExists(configuration,
                                               ipaddress, isItTcpConnection);
}

static void createPTRQuestion(unsigned char *responseData, unsigned char *buffer, ssize_t *length) {
    unsigned char *qname;
    qname = (unsigned char *) &buffer[sizeof(struct DNS_HEADER)];
    aton(responseData, qname);
    struct QUESTION *qinfo = NULL;
    qinfo = (struct QUESTION *) &buffer[sizeof(struct DNS_HEADER) + (strlen((const char *) qname) + 1)]; //fill it
    qinfo->qtype = htons(T_A);
    qinfo->qclass = htons(1);
    *length = sizeof(struct DNS_HEADER) + (strlen((const char *) qname) + 1) + sizeof(struct QUESTION);
}

static void createOtherQuestion(unsigned char *name, unsigned char *buffer, ssize_t *length) {
    unsigned char *qname;
    create_mdns_header(buffer, 0);
    qname = (unsigned char *) &buffer[sizeof(struct DNS_HEADER)];
    aton(name, qname);
    struct QUESTION *qinfo = NULL;
    qinfo = (struct QUESTION *) &buffer[sizeof(struct DNS_HEADER) + (strlen((const char *) qname) + 1)];
    qinfo->qtype = htons(T_PTR);
    qinfo->qclass = htons(1);
    *length = sizeof(struct DNS_HEADER) + (strlen((const char *) qname) + 1) + sizeof(struct QUESTION);
}

int MDNSApiCreateQuestion(unsigned char *name, unsigned char *responseData, int type, unsigned char *buffer,
                    ssize_t *length, struct UserConfiguration *configuration) {
    if (type == T_A && (
            strstr(name, utilNameOpoznieniaUDP) || strstr(name, utilNameSshTCP)
    )) {
        uint32_t *p;
        p = (uint32_t *) responseData;
        struct sockaddr_in addedHostAddress;
        addedHostAddress.sin_addr.s_addr = (*p);
        if (strstr(name, utilNameOpoznieniaUDP)) {
            //this means that this is opoznienia_udp util
            create_or_add(*p, "udp", configuration, false);
        } else {
            if (strstr(name, utilNameSshTCP)) {
                //this means that this is tcp util
                create_or_add(*p, "tcp", configuration, true);
            }
        }
        return 0;
    } else if (type == T_PTR && (!strcmp(name, utilNameOpoznieniaUDP) || !strcmp(name, utilNameSshTCP))) {
        create_mdns_header(buffer, 0);
        createPTRQuestion(responseData, buffer, length);
        return 1;
    } else if (type == 0) {
        createOtherQuestion(name, buffer, length);
    }
    return 0;
}

struct QUERY *MDNSApigetQuestion(char *buffer) {
    struct DNS_HEADER *dns = NULL;
    unsigned char *reader;
    int i = 0;
    int j = 0;
    int stop = 0;
    dns = (struct DNS_HEADER *) buffer;
    struct QUERY *questions = malloc(20 * sizeof(struct QUERY));
    reader = &buffer[sizeof(struct DNS_HEADER)];
    stop = 0;
    for (i = 0; i < ntohs(dns->q_count); i++) {
        questions[i].name = ReadName(reader, buffer, &stop);
        reader = reader + stop;
        questions[i].ques = (struct QUESTION *) (reader);
        reader = reader + sizeof(struct QUESTION);
    }
    return questions;
}

static void parseSingleAnswer(char *buffer, int i, int j, int *stop,
                       struct RES_RECORD *answers, unsigned char *reader) {
    answers[i].name = ReadName(reader, buffer, stop);
    reader = reader + (*stop);
    answers[i].resource = (struct R_DATA *) (reader);
    reader = reader + sizeof(struct R_DATA);
    if (ntohs(answers[i].resource->type) == T_A) {
        answers[i].rdata = (unsigned char *) malloc(ntohs(answers[i].resource->data_len));
        for (j = 0; j < ntohs(answers[i].resource->data_len); j++) {
            answers[i].rdata[j] = reader[j];
        }
        answers[i].rdata[ntohs(answers[i].resource->data_len)] = '\0';
        reader = reader + ntohs(answers[i].resource->data_len);
    } else {
        answers[i].rdata = ReadName(reader, buffer, stop);
        reader = reader + (*stop);
    }
}

struct RES_RECORD *MDNSApigetAnswer(char *buffer) {
    int i = 0;
    int j = 0;
    int stop = 0;
    struct RES_RECORD *answers = malloc(20 * sizeof(struct RES_RECORD));
    struct DNS_HEADER *dns = NULL;
    unsigned char *reader;
    dns = (struct DNS_HEADER *) buffer;
    reader = &buffer[sizeof(struct DNS_HEADER)];
    for (i = 0; i < ntohs(dns->ans_count); i++) {
        parseSingleAnswer(buffer, i, j, &stop, answers, reader);
    }
    return answers;
}

static void processAuthorities(char *buffer, int i, struct DNS_HEADER *mdnsHeader,
                               struct RES_RECORD *authority, int *stop, unsigned char **nameOfReader) {
    for (i = 0; i < ntohs(mdnsHeader->auth_count); i++) {
        authority[i].name = ReadName((*nameOfReader), buffer, stop);
        (*nameOfReader) += (*stop);
        authority[i].resource = (struct R_DATA *) (*nameOfReader);
        (*nameOfReader) += sizeof(struct R_DATA);
        authority[i].rdata = ReadName((*nameOfReader), buffer, stop);
        (*nameOfReader) += (*stop);
    }
}

static void processAnswers(char *buffer, int j, struct DNS_HEADER *mdnsHeader,
                    struct RES_RECORD *answers, int *i, int *stop,
                    unsigned char **nameOfReader) {
    for ((*i) = 0; (*i) < ntohs(mdnsHeader->ans_count); (*i)++) {
        answers[(*i)].name = ReadName((*nameOfReader), buffer, stop);
        (*nameOfReader) = (*nameOfReader) + (*stop);
        answers[(*i)].resource = (struct R_DATA *) (*nameOfReader);
        (*nameOfReader) = (*nameOfReader) + sizeof(struct R_DATA);
        if (ntohs(answers[(*i)].resource->type) == T_A) {
            answers[(*i)].rdata = (unsigned char *) malloc(ntohs(answers[(*i)].resource->data_len));
            for (j = 0; j < ntohs(answers[(*i)].resource->data_len); j++) {
                answers[(*i)].rdata[j] = (*nameOfReader)[j];
            }
            answers[(*i)].rdata[ntohs(answers[(*i)].resource->data_len)] = '\0';
            (*nameOfReader) = (*nameOfReader) + ntohs(answers[(*i)].resource->data_len);
        } else {
            answers[(*i)].rdata = ReadName((*nameOfReader), buffer, stop);
            (*nameOfReader) = (*nameOfReader) + (*stop);
        }
    }
}

static void processAdditionals(char *buffer, int i, int j, int *stop, unsigned char *nameOfReader,
                        struct DNS_HEADER *mdnsHeader, struct RES_RECORD *additional) {
    for (i = 0; i < ntohs(mdnsHeader->add_count); i++) {
        additional[i].name = ReadName(nameOfReader, buffer, stop);
        nameOfReader += (*stop);
        additional[i].resource = (struct R_DATA *) (nameOfReader);
        nameOfReader += sizeof(struct R_DATA);
        if (ntohs(additional[i].resource->type) == T_A) {
            additional[i].rdata = (unsigned char *) malloc(ntohs(additional[i].resource->data_len));
            for (j = 0; j < ntohs(additional[i].resource->data_len); j++)
                additional[i].rdata[j] = nameOfReader[j];

            additional[i].rdata[ntohs(additional[i].resource->data_len)] = '\0';
            nameOfReader += ntohs(additional[i].resource->data_len);
        } else {
            additional[i].rdata = ReadName(nameOfReader, buffer, stop);
            nameOfReader += (*stop);
        }
    }
}

void MDNSApiParseMessage(char *buffer) {
    int i = 0;
    int j = 0;
    int stop = 0;
    unsigned char *questionName, *nameOfReader;
    struct sockaddr_in addressOfSocket;
    struct DNS_HEADER *mdnsHeader = NULL;
    struct RES_RECORD answers[20];
    struct RES_RECORD authority[20];
    struct RES_RECORD additional[20];
    mdnsHeader = (struct DNS_HEADER *) buffer;
    questionName = (unsigned char *) &buffer[sizeof(struct DNS_HEADER)];
    nameOfReader = &buffer[sizeof(struct DNS_HEADER)];
    stop = 0;
    processAnswers(buffer, j, mdnsHeader, answers, &i, &stop, &nameOfReader);
    processAuthorities(buffer, i, mdnsHeader, authority, &stop, &nameOfReader);
    processAdditionals(buffer, i, j, &stop, nameOfReader, mdnsHeader, additional);
    for (i = 0; i < ntohs(mdnsHeader->ans_count); i++) {
        if (ntohs(answers[i].resource->type) == T_A) {
            long *p;
            p = (long *) answers[i].rdata;
            addressOfSocket.sin_addr.s_addr = (*p);
        }
    }
    for (i = 0; i < ntohs(mdnsHeader->add_count); i++) {
        if (ntohs(additional[i].resource->type) == 1) {
            long *p;
            p = (long *) additional[i].rdata;
            addressOfSocket.sin_addr.s_addr = (*p);
        }
    }
    return;
}

//code based on this site: http://www.binarytides.com/dns-query-code-in-c-with-linux-sockets/
u_char *ReadName(unsigned char *reader, unsigned char *buffer, int *count) {
    unsigned char *name;
    unsigned int p = 0, jumped = 0, offset;
    int i, j;
    *count = 1;
    name = (unsigned char *) malloc(256);
    name[0] = '\0';
    while (*reader != 0) {
        if (*reader >= 192) {
            offset = (*reader) * 256 + *(reader + 1) - 49152;
            reader = buffer + offset - 1;
            jumped = 1;
        } else {
            name[p++] = *reader;
        }
        reader = reader + 1;
        if (jumped == 0) {
            *count = *count + 1;
        }
    }
    name[p] = '\0';
    if (jumped == 1) {
        *count = *count + 1;
    }
    for (i = 0; i < (int) strlen((const char *) name); i++) {
        p = name[i];
        for (j = 0; j < (int) p; j++) {
            name[i] = name[i + 1];
            i = i + 1;
        }
        name[i] = '.';
    }
    name[i] = '\0';
    return name;
}