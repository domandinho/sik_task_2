#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "err.h"
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <time.h>
#include <sys/time.h>

#define __STDC_FORMAT_MACROS

#include <inttypes.h>
#include "countmicrosecondsservice.h"
#include "config.h"
#include "delaytimerudpserver.h"

void serviceUDPServer(struct UserConfiguration configuration) {
    int sock;
    int flags, sflags;
    struct sockaddr_in server_address;
    struct sockaddr_in client_address;
    socklen_t snda_len, rcva_len;
    ssize_t len, snd_len;
    int anslength;
    int16_t port = configuration.UDPMeasureDelayTimeServerPort;
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
        syserr("socket");
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(port);
    if (bind(sock, (struct sockaddr *) &server_address,
             (socklen_t) sizeof(server_address)) < 0)
        syserr("bind");
    snda_len = (socklen_t) sizeof(client_address);
    for (; ;) {
        do {
            rcva_len = (socklen_t) sizeof(client_address);
            flags = 0;
            uint64_t receivedData;
            len = recvfrom(sock, &receivedData, sizeof(receivedData), flags,
                           (struct sockaddr *) &client_address, &rcva_len);
            if (len < 0)
                syserr("error on datagram from client socket");
            else {
                uint64_t actualData = countMicroseconds();
                uint64_t answer[2];
                answer[0] = receivedData;
                answer[1] = actualData;
                answer[1] = htobe64(answer[1]);
                anslength = 2 * sizeof(uint64_t);
                sflags = 0;
                snd_len = sendto(sock, answer, (size_t) anslength, sflags,
                                 (struct sockaddr *) &client_address, snda_len);
                if (snd_len != anslength)
                    syserr("error on sending datagram to client socket");
            }
        } while (len > 0);
    }
}

void *threadServiceUDPServer(void *baseConfiguration) {
    struct UserConfiguration *pointerToDataWhichWillBeFreedNow = (struct UserConfiguration *) baseConfiguration;
    struct UserConfiguration configuration = *pointerToDataWhichWillBeFreedNow;
    free(pointerToDataWhichWillBeFreedNow);
    serviceUDPServer(configuration);
    return NULL;
}

void runServiceUDPServer(struct UserConfiguration *baseConfiguration) {
    pthread_t t;
    struct UserConfiguration *configuration = malloc(sizeof(struct UserConfiguration));
    if (!configuration) {
        syserr("malloc");
    }
    *configuration = *baseConfiguration;
    int receiveAnswerCode = receiveAnswerCode = pthread_create(&t, 0, threadServiceUDPServer, configuration);
    if (receiveAnswerCode == -1) {
        syserr("pthread_create");
    }
    receiveAnswerCode = pthread_detach(t);
    if (receiveAnswerCode == -1) {
        syserr("pthread_detach");
    }
}
