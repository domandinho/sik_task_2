#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "delaycounter.h"
#include "config.h"
#include "delaytimertcp.h"
#include "delaytimerudp.h"
#include "delaytimericmp.h"
#include "err.h"
#include "mdnsapi.h"
#include "mdnsserver.h"

void *handleDelayCountingTCP(void *config) {
    struct UserConfiguration *pointerToDataWhichWillBeFreedNow = (struct UserConfiguration *) config;
    struct UserConfiguration configuration = *pointerToDataWhichWillBeFreedNow;
    free(pointerToDataWhichWillBeFreedNow);
    serviceTCPClient(configuration);
    return NULL;
}

void *handleDelayCountingUDP(void *config) {
    struct UserConfiguration *pointerToDataWhichWillBeFreedNow = (struct UserConfiguration *) config;
    struct UserConfiguration configuration = *pointerToDataWhichWillBeFreedNow;
    free(pointerToDataWhichWillBeFreedNow);
    serviceUDPClient(configuration);
    return NULL;
}

void *handleDelayCountingICMP(void *config) {
    struct UserConfiguration *pointerToDataWhichWillBeFreedNow = (struct UserConfiguration *) config;
    struct UserConfiguration configuration = *pointerToDataWhichWillBeFreedNow;
    free(pointerToDataWhichWillBeFreedNow);
    serviceICMPClient(configuration);
    return NULL;
}


void runThread(char *host, int port, int connectionType,
               struct UserConfiguration *baseConfiguration) {
    pthread_t t;
    struct UserConfiguration *configuration = malloc(sizeof(struct UserConfiguration));
    if (!configuration) {
        syserr("malloc");
    }
    *configuration = *baseConfiguration;
    sprintf(configuration->DelayCounterHostname, "%s", host);
    configuration->DelayCounterPort = port;
    int receiveAnswerCode = 0;
    switch (connectionType) {
        case 1:
            receiveAnswerCode = pthread_create(&t, 0, handleDelayCountingTCP, configuration);
            break;
        case 2:
            receiveAnswerCode = pthread_create(&t, 0, handleDelayCountingUDP, configuration);
            break;
        case 3:
            receiveAnswerCode = pthread_create(&t, 0, handleDelayCountingICMP, configuration);
            break;
        default:
            free(configuration);
            break;
    }
    if (receiveAnswerCode == -1) {
        syserr("pthread_create");
    }
    receiveAnswerCode = pthread_detach(t);
    if (receiveAnswerCode == -1) {
        syserr("pthread_detach");
    }
}

void runDelayCounting(struct UserConfiguration *configuration,
                      char *hostname, char *port) {
    int i;
    for (i = 1; i <= 3; i++) {
        int portNumber = atoi(port);
        if (i == 1) {
            portNumber = 22;
        }
        runThread(hostname, portNumber,
                  i, configuration);
    }
}

bool isThisHostProviderForBothOfUtils(char *hostname,
                                      struct DelayStructuresList *list) {
    if (isElementExistsOnList(hostname, list) == false) {
        return false;
    } else {
        struct DelayStructure *element = search_in_list(hostname, NULL, list);
        if (element->TCPdelay[0] == 1 && element->TCPdelay[1] == 1) {
            return true;
        } else {
            return false;
        }
    }
}

void deleteHostsFromDelayCounterListWhichAreAbsentOnMDNSList(
        struct UserConfiguration *configuration) {
    showListContent(configuration->DelayCounterHostList);
    struct DelayStructure *iterator = configuration->DelayCounterHostList->head;
    int i = 0;
    int numberOfElements = configuration->DelayCounterHostList->size;
    for (i = 0; i < numberOfElements; i++) {
        if (isThisHostProviderForBothOfUtils(iterator->hostname,
                                             configuration->MDNSServerHostList) == false) {
            char element[15];
            strcpy(element, iterator->hostname);
            iterator = iterator->next;
            delete_from_list(element, configuration->DelayCounterHostList);
        } else {
            iterator = iterator->next;
        }
    }
}

void insertElementsFromMDNSListToDelayCounterList(
        struct UserConfiguration *configuration) {
    struct DelayStructure *iterator = configuration->MDNSServerHostList->head;
    showListContent(configuration->DelayCounterHostList);
    int i = 0;
    for (i = 0; i < configuration->MDNSServerHostList->size; i++) {
        if (isElementExistsOnList(iterator->hostname,
                                  configuration->DelayCounterHostList) == false
            && isThisHostProviderForBothOfUtils(iterator->hostname, configuration->MDNSServerHostList)) {
            add_to_list(false, configuration->DelayCounterHostList);
            strcpy(configuration->DelayCounterHostList->head->hostname,
                   iterator->hostname);
            strcpy(configuration->DelayCounterHostList->head->port,
                   iterator->port);
            runDelayCounting(configuration, iterator->hostname,
                             iterator->port);
            showListContent(configuration->DelayCounterHostList);
        }
        showListContent(configuration->DelayCounterHostList);
        iterator = iterator->next;
    }
    clearList(configuration->MDNSServerHostList);
}

void synchronizeListsOfHosts(struct UserConfiguration *configuration) {
    pthread_mutex_lock(configuration->DelayCounterHostListMutex);
    {
        pthread_mutex_lock(configuration->MDNSServerHostListMutex);
        {
            deleteHostsFromDelayCounterListWhichAreAbsentOnMDNSList(configuration);
            insertElementsFromMDNSListToDelayCounterList(configuration);
        }
        pthread_mutex_unlock(configuration->MDNSServerHostListMutex);
    }
    pthread_mutex_unlock(configuration->DelayCounterHostListMutex);
}

void runThreadRunner(struct UserConfiguration *configuration) {
    socklen_t lengthOdSocketAddress = sizeof(struct sockaddr);
    int identificatorOfSocket, flags = 0;
    unsigned char sendingMessageBufferUDP[MAX_BUF_SIZE];
    unsigned char sendingMessageBufferTCP[MAX_BUF_SIZE];
    int socketOption = 1;
    ssize_t lengthOfSendedMessage, lengthOfUDPMessage, lengthOfTCPMessage;
    struct sockaddr addressOfMDNSServer;
    ((struct sockaddr_in *) &addressOfMDNSServer)->sin_family = AF_INET;
    ((struct sockaddr_in *) &addressOfMDNSServer)->sin_addr.s_addr = inet_addr("224.0.0.251");
    ((struct sockaddr_in *) &addressOfMDNSServer)->sin_port = htons(5353);
    setMyOwnIpAddress();
    setMyOwnHostName();
    identificatorOfSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (identificatorOfSocket < 0) {
        syserr("socket");
    }
    setsockopt(identificatorOfSocket, SOL_SOCKET, SO_REUSEADDR, &socketOption, sizeof(socketOption));
    if (bind(identificatorOfSocket, &addressOfMDNSServer, lengthOdSocketAddress) < 0) {
        syserr("bind");
    }
    runServiceDNSReceiver(configuration, identificatorOfSocket);
    MDNSApiCreateQuestion("_opoznienia._udp.local.", "x", 0, sendingMessageBufferUDP,
                          &lengthOfUDPMessage, configuration);
    MDNSApiCreateAnswer("_ssh._tcp.local.", T_PTR, sendingMessageBufferTCP, &lengthOfTCPMessage);
    while (true) {
        synchronizeListsOfHosts(configuration);
        lengthOfSendedMessage = sendto(identificatorOfSocket, sendingMessageBufferUDP, lengthOfUDPMessage, flags,
                                       &addressOfMDNSServer,
                                       lengthOdSocketAddress);
        if (lengthOfSendedMessage != lengthOfUDPMessage) {
            syserr("sendto");
        }
        if (configuration->DNSdelayMode) {
            lengthOfSendedMessage = sendto(identificatorOfSocket, sendingMessageBufferTCP, lengthOfTCPMessage, flags,
                                           &addressOfMDNSServer,
                                           lengthOdSocketAddress);
            if (lengthOfSendedMessage != lengthOfTCPMessage) {
                syserr("sendto");
            }
        }
        sleep(configuration->measureDetectingServersInterval);
    }
    if (close(identificatorOfSocket) == -1) {
        syserr("close");
    }
}