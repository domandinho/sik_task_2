//
// Created by domandinho on 31.05.15.
//

#ifndef SIECI_LISTIMPLEMENTATION_H
#define SIECI_LISTIMPLEMENTATION_H

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include "config.h"


struct DelayStructuresList {
    struct DelayStructure *head;
    struct DelayStructure *curr;
    int size;
};

struct DelayStructure {
    char hostname[15];
    char port[8];
    int UDPdelay[10];
    int TCPdelay[10];
    int ICMPdelay[10];
    struct DelayStructure *next;
};

struct DelayStructure *add_to_list(bool add_to_end, struct DelayStructuresList *list);

struct DelayStructure *search_in_list(char *hostname, struct DelayStructure **prev, struct DelayStructuresList *list);

bool isElementExistsOnList(char* hostname, struct DelayStructuresList* list);

int delete_from_list(char *hostname, struct DelayStructuresList *list);

void print_list(struct DelayStructuresList *list, char *text, int *begin);

void popFrontList(struct DelayStructuresList *list);

void clearList(struct DelayStructuresList *list);

void showListContent(struct DelayStructuresList *list);

void bubbleSort(struct DelayStructuresList *list);

#endif //SIECI_LISTIMPLEMENTATION_H
