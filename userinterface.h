#ifndef USERINTERFACE_H_
#define USERINTERFACE_H_

#include "config.h"

void serviceUserInterface(struct UserConfiguration configuration);

void runUserInterfaceModule(struct UserConfiguration *baseConfiguration);

#endif /* USERINTERFACE_H_ */
