/*
 * delaytimerudp.h
 *
 *  Created on: 22 maj 2015
 *      Author: domandinho
 */

#ifndef DELAYTIMERUDP_H_
#define DELAYTIMERUDP_H_

#include "config.h"
void serviceUDPClient(struct UserConfiguration configuration);

#endif /* DELAYTIMERUDP_H_ */
