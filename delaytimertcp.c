#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>

#define __STDC_FORMAT_MACROS

#include <inttypes.h>
#include <fcntl.h>
#include <math.h>

#define BUFFER_SIZE 1000


#include "delaytimertcp.h"
#include "err.h"
#include "countmicrosecondsservice.h"
#include "delaycounterhostlistapi.h"

void countDelay(struct UserConfiguration *configuration, int whichCount, int TCPsock,
                struct addrinfo *TCPinformationsAboutServerAddress, int *valopt, socklen_t *lengthOfSocket,
                fd_set *myset) {
    uint64_t timeBefore = countMicroseconds();
    int delay = 0;
    int resultOfConnection = connect(TCPsock, TCPinformationsAboutServerAddress->ai_addr,
                                     TCPinformationsAboutServerAddress->ai_addrlen);
    if (resultOfConnection < 0) {
        if (errno == EINPROGRESS) {
            struct timeval timeInSeconds;
            timeInSeconds.tv_sec = 10;
            timeInSeconds.tv_usec = 0;
            FD_ZERO(myset);
            FD_SET(TCPsock, myset);
            resultOfConnection = select(TCPsock + 1, NULL, myset, NULL, &timeInSeconds);
            if (resultOfConnection < 0 && errno != EINTR) {
                fprintf(stderr, "Error connecting %d - %s\n",
                        errno, strerror(errno));
                return;
            }
            else if (resultOfConnection > 0) {
                (*lengthOfSocket) = sizeof(int);
                if (getsockopt(TCPsock, SOL_SOCKET, SO_ERROR, (void *) (valopt), lengthOfSocket) < 0) {
                    syserr("Error in getsockopt() %d - %s\n", errno, strerror(errno));
                }
                if ((*valopt)) {
                    fprintf(stderr,
                            "Error in delayed connection() %d - %s\n",
                            (*valopt), strerror((*valopt)));
                    return;
                }
            }
        }
        else {
            fprintf(stderr, "Error connecting %d - %s\n", errno, strerror(errno));
        }
    }
    uint64_t timeAfter = countMicroseconds();
    delay = timeAfter - timeBefore;
    if (delay > 100000000) {
        delay = 100000000;
    }
    DelayCounterInsertDelayStatistic(configuration, 1, delay, whichCount);
}

void createSocket(struct UserConfiguration *configuration, int whichCount) {
    int TCPsock;
    struct addrinfo TCPinformationsAboutAddress;
    struct addrinfo *TCPinformationsAboutServerAddress;
    int TCPerrorInformation;
    long socketSettings;
    int valopt;
    socklen_t lengthOfSocket;
    fd_set myset;
    memset(&TCPinformationsAboutAddress, 0, sizeof(struct addrinfo));
    TCPinformationsAboutAddress.ai_family = AF_INET; // IPv4
    TCPinformationsAboutAddress.ai_socktype = SOCK_STREAM;
    TCPinformationsAboutAddress.ai_protocol = IPPROTO_TCP;
    char portNumber[15];
    sprintf(portNumber, "%d", configuration->DelayCounterPort);
    TCPerrorInformation = getaddrinfo(configuration->DelayCounterHostname, portNumber,
                                      &TCPinformationsAboutAddress,
                                      &TCPinformationsAboutServerAddress);
    if (TCPerrorInformation != 0)
        syserr("getaddrinfo: %s\n", gai_strerror(TCPerrorInformation));
    TCPsock = socket(TCPinformationsAboutServerAddress->ai_family,
                     TCPinformationsAboutServerAddress->ai_socktype,
                     TCPinformationsAboutServerAddress->ai_protocol);
    if (TCPsock < 0) {
        syserr("socket");
    }
    if ((socketSettings = fcntl(TCPsock, F_GETFL, NULL)) < 0) {
        syserr("Error fcntl(..., F_GETFL) (%s)\n", strerror(errno));

    }
    socketSettings |= O_NONBLOCK;
    if (fcntl(TCPsock, F_SETFL, socketSettings) < 0) {
        syserr("Error fcntl(..., F_SETFL) (%s)\n", strerror(errno));
    }
    countDelay(configuration, whichCount, TCPsock, TCPinformationsAboutServerAddress, &valopt, &lengthOfSocket, &myset);
    if ((socketSettings = fcntl(TCPsock, F_GETFL, NULL)) < 0) {
        syserr("Error fcntl(..., F_GETFL) (%s)\n", strerror(errno));
    }
    socketSettings &= (~O_NONBLOCK);
    if (fcntl(TCPsock, F_SETFL, socketSettings) < 0) {
        syserr("Error fcntl(..., F_SETFL) (%s)\n", strerror(errno));
    }
    freeaddrinfo(TCPinformationsAboutServerAddress);
    if (close(TCPsock) == -1) {
        fprintf(stderr, "close");
    }
}

void serviceTCPClient(struct UserConfiguration configuration) {
    int whichCount = 0;
    while (DelayCounterIsHostExistsInList(&configuration) == true) {
        createSocket(&configuration, whichCount);
        whichCount++;
        sleepSeconds(configuration.measureDelayTimeIntervals);
    }
}