#include "delaycounterhostlistapi.h"
#include "listimplementation.h"
#include "config.h"
#include <string.h>
#include <stdbool.h>

void UserInterfacePrintQueue(struct UserConfiguration *configuration, char *text, int *begin) {
    pthread_mutex_lock(configuration->DelayCounterHostListMutex);
    {
        print_list(configuration->DelayCounterHostList, text, begin);
    }
    pthread_mutex_unlock(configuration->DelayCounterHostListMutex);
}

void DelayCounterInsertDelayStatistic(struct UserConfiguration *configuration, int connectionType,
                                      int delay, int whichCount) {
    pthread_mutex_lock(configuration->DelayCounterHostListMutex);
    {
        struct DelayStructure *actual = search_in_list(
                configuration->DelayCounterHostname,
                NULL, configuration->DelayCounterHostList);
        if (actual == NULL) {
            pthread_mutex_unlock(configuration->DelayCounterHostListMutex);
            return;
        }
        switch (connectionType) {
            case 1:
                actual->TCPdelay[(whichCount) % 10] = delay;
                break;
            case 2:
                actual->UDPdelay[(whichCount) % 10] = delay;
                break;
            case 3:
                actual->ICMPdelay[(whichCount) % 10] = delay;
                break;
            default:
                break;
        }
    }
    pthread_mutex_unlock(configuration->DelayCounterHostListMutex);
}

bool DelayCounterIsHostExistsInList(struct UserConfiguration *configuration) {
    bool answer = false;
    pthread_mutex_lock(configuration->DelayCounterHostListMutex);
    {
        if (search_in_list(configuration->DelayCounterHostname, NULL,
                           configuration->DelayCounterHostList

        ) != NULL) {
            answer = true;
        }
    }
    pthread_mutex_unlock(configuration->DelayCounterHostListMutex);
    return answer;
}

static void setUtilsInformationToHost(struct DelayStructure *structure,
                                      bool isItTcpConnection) {
    if (isItTcpConnection == true) {
        structure->TCPdelay[1] = 1;
    } else {
        structure->TCPdelay[0] = 1;
    }
}

void insertRecordToMDNSListOnlyIfItDoesntExists(
        struct UserConfiguration *configuration, char *ipAddress, bool isItTcpConnection) {
    pthread_mutex_lock(configuration->MDNSServerHostListMutex);
    {
        if (isElementExistsOnList(
                ipAddress, configuration->MDNSServerHostList) == true) {
            struct DelayStructure *element = search_in_list(
                    ipAddress, NULL, configuration->MDNSServerHostList);
            setUtilsInformationToHost(element, isItTcpConnection);
            pthread_mutex_unlock(configuration->MDNSServerHostListMutex);
            return;
        }
        add_to_list(false, configuration->MDNSServerHostList);
        struct DelayStructure *element = configuration->MDNSServerHostList->head;
        sprintf(element->hostname, "%s", ipAddress);
        sprintf(element->port, "%d", configuration->UDPMeasureDelayTimeServerPort);
        setUtilsInformationToHost(element, isItTcpConnection);
        showListContent(configuration->MDNSServerHostList);
    }
    pthread_mutex_unlock(configuration->MDNSServerHostListMutex);
}