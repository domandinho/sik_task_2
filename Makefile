TARGET: opoznienia
CC	= cc
CFLAGS	= -Wall -O2 -g
LFLAGS	= -Wall
ALLHEADERS = delaytimerudpserver.o delaytimerudp.o delaycounter.o mdnsserver.o delaytimericmp.o delaytimertcp.o config.o countmicrosecondsservice.o userinterface.o listimplementation.o delaycounterhostlistapi.o mdnsapi.o err.o
opoznienia: opoznienia.o $(ALLHEADERS)
	$(CC) $(LFLAGS) $^ -o $@ -lpthread
.PHONY: clean TARGET
clean:
	rm -f opoznienia *.o *~ *.bak
