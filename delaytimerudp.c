#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#include <inttypes.h>
#include <sys/cdefs.h>

#define __STDC_FORMAT_MACROS


#define BUFFER_SIZE 1000


#include "delaytimerudp.h"
#include "err.h"
#include "countmicrosecondsservice.h"
#include "config.h"
#include "delaycounterhostlistapi.h"

static void setAddrInfo(struct addrinfo *UDPinformationsAboutAddress) {
    (void) memset(UDPinformationsAboutAddress, 0, sizeof(struct addrinfo));
    UDPinformationsAboutAddress->ai_family = AF_INET; // IPv4
    UDPinformationsAboutAddress->ai_socktype = SOCK_DGRAM;
    UDPinformationsAboutAddress->ai_protocol = IPPROTO_UDP;
    UDPinformationsAboutAddress->ai_flags = 0;
    UDPinformationsAboutAddress->ai_addrlen = 0;
    UDPinformationsAboutAddress->ai_addr = NULL;
    UDPinformationsAboutAddress->ai_canonname = NULL;
    UDPinformationsAboutAddress->ai_next = NULL;
}

void singleDelay(int UDPidOfSocket, struct sockaddr_in *my_address,
                 struct sockaddr_in *srvr_address,
                 struct UserConfiguration *configuration, int whichCount) {
    uint64_t actualData = countMicroseconds();
    socklen_t UDPrcva_len;
    int UDPreceiveFlags, UDPsendFlags;
    size_t UDPlen;
    ssize_t UDPlengthOfSendedMessage, UDPlengthOfReceivedMessage;
    actualData = htobe64(actualData);
    UDPlen = sizeof(uint64_t);
    UDPsendFlags = 0;
    UDPrcva_len = (socklen_t) sizeof((*my_address));
    UDPlengthOfSendedMessage = sendto(UDPidOfSocket, (void *) &actualData,
                                      UDPlen, UDPsendFlags, (struct sockaddr *) my_address, UDPrcva_len);
    if (UDPlengthOfSendedMessage != (ssize_t) UDPlen) {
        syserr("partial / failed write");
    }
    UDPreceiveFlags = 0;
    UDPlen = 2 * (size_t) sizeof(uint64_t);
    UDPrcva_len = (socklen_t) sizeof((*srvr_address));
    uint64_t tab[2];
    UDPlengthOfReceivedMessage = recvfrom(UDPidOfSocket, tab, UDPlen,
                                          UDPreceiveFlags, (struct sockaddr *) srvr_address, &UDPrcva_len);
    uint64_t now = countMicroseconds();
    tab[0] = be64toh(tab[0]);
    tab[1] = be64toh(tab[1]);
    int delay = now - tab[0];
    if (UDPlengthOfReceivedMessage < 0) {
        delay = 10000000;
    }
    DelayCounterInsertDelayStatistic(configuration, 2, delay, whichCount);
}

void serviceUDPClient(struct UserConfiguration configuration) {
    int whichCount = 0;
    while (DelayCounterIsHostExistsInList(&configuration) == true) {
        int UDPidOfSocket;
        struct addrinfo UDPinformationsAboutAddress;
        struct addrinfo *UDPinformationsAboutServerAddress;
        struct sockaddr_in my_address;
        struct sockaddr_in srvr_address;
        setAddrInfo(&UDPinformationsAboutAddress);
        if (getaddrinfo(configuration.DelayCounterHostname,
                        NULL, &UDPinformationsAboutAddress,
                        &UDPinformationsAboutServerAddress) != 0) {
            syserr("getaddrinfo");
        }
        my_address.sin_family = AF_INET;
        my_address.sin_addr.s_addr =
                ((struct sockaddr_in *) (UDPinformationsAboutServerAddress->ai_addr))->sin_addr.s_addr;
        my_address.sin_port = htons((uint16_t) configuration.DelayCounterPort);
        freeaddrinfo(UDPinformationsAboutServerAddress);
        UDPidOfSocket = socket(PF_INET, SOCK_DGRAM, 0);
        if (UDPidOfSocket < 0) {
            syserr("socket");
        }
        struct timeval timeout = {10, 0};
        setsockopt(UDPidOfSocket, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout, sizeof(struct timeval));
        singleDelay(UDPidOfSocket, &my_address,
                    &srvr_address, &configuration, whichCount);
        whichCount++;
        sleepSeconds(configuration.measureDelayTimeIntervals);
        if (close(UDPidOfSocket) == -1) {
            syserr("close");
        };
    }
}


