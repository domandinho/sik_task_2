/*
 * delaytimertcp.h
 *
 *  Created on: 22 maj 2015
 *      Author: domandinho
 */

#ifndef DELAYTIMERTCP_H_
#define DELAYTIMERTCP_H_

#include "config.h"

void serviceTCPClient(struct UserConfiguration configuration);

#endif /* DELAYTIMERTCP_H_ */
