/*
 * countmicrosecondsservice.h
 *
 *  Created on: 26 maj 2015
 *      Author: domandinho
 */

#ifndef COUNTMICROSECONDSSERVICE_H_
#define COUNTMICROSECONDSSERVICE_H_
#include <inttypes.h>
uint64_t countMicroseconds();

#endif /* COUNTMICROSECONDSSERVICE_H_ */
