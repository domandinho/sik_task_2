//
// Created by domandinho on 31.05.15.
//

#ifndef SIECI_DELAYCOUNTERHOSTLISTAPI_H
#define SIECI_DELAYCOUNTERHOSTLISTAPI_H

#include "config.h"
#include <stdbool.h>

void UserInterfacePrintQueue(struct UserConfiguration *configuration, char *text, int *begin);

void DelayCounterInsertDelayStatistic(struct UserConfiguration *configuration, int connectionType,
                                      int delay, int whichCount);

bool DelayCounterIsHostExistsInList(struct UserConfiguration *configuration);

void insertRecordToMDNSListOnlyIfItDoesntExists(
        struct UserConfiguration *configuration, char *ipAddress, bool isItTcpConnection);

#endif //SIECI_DELAYCOUNTERHOSTLISTAPI_H
