#include "delaytimericmp.h"
#include "countmicrosecondsservice.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
// this sets ICMP declaration
#include <netinet/ip_icmp.h>
#include "err.h"
#include <pwd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include "config.h"
#include "delaycounterhostlistapi.h"

#define BSIZE 1000
#define ICMP_HEADER_LEN 8
#define NOBODY_UID_GID 99

/* source: libfree */
unsigned short in_cksum(unsigned short *addr, int len) {
    int nleft = len;
    int sum = 0;
    unsigned short *w = addr;
    unsigned short answer = 0;
    /*
     * Our algorithm is simple, using a 32 bit accumulator (sum), we add
     * sequential 16 bit words to it, and at the end, fold back all the
     * carry bits from the top 16 bits into the lower 16 bits.
     */
    while (nleft > 1) {
        sum += *w++;
        nleft -= 2;
    }
    /* 4mop up an odd byte, if necessary */
    if (nleft == 1) {
        *(unsigned char *) (&answer) = *(unsigned char *) w;
        sum += answer;
    }
    /* 4add back carry outs from top 16 bits to low 16 bits */
    sum = (sum >> 16) + (sum & 0xffff); /* add hi 16 to low 16 */
    sum += (sum >> 16); /* add carry */
    answer = ~sum; /* truncate to 16 bits */
    return (answer);
}

void send_ping_request(int sock, char *s_send_addr, int *timeBefore,
                       int *timeAfter) {
    struct addrinfo addr_hints;
    struct addrinfo *addr_result;
    struct sockaddr_in send_addr;
    struct icmp *icmp;
    char send_buffer[BSIZE];
    int err = 0;
    ssize_t data_len = 0;
    ssize_t icmp_len = 0;
    ssize_t len = 0;
    // 'converting' host/port in string to struct addrinfo
    memset(&addr_hints, 0, sizeof(struct addrinfo));
    addr_hints.ai_family = AF_INET;
    addr_hints.ai_socktype = SOCK_RAW;
    addr_hints.ai_protocol = IPPROTO_ICMP;
    err = getaddrinfo(s_send_addr, 0, &addr_hints, &addr_result);
    if (err != 0)
        syserr("getaddrinfo: %s\n", gai_strerror(err));
    send_addr.sin_family = AF_INET;
    send_addr.sin_addr.s_addr =
            ((struct sockaddr_in *) (addr_result->ai_addr))->sin_addr.s_addr;
    send_addr.sin_port = htons(0);
    freeaddrinfo(addr_result);
    memset(send_buffer, 0, sizeof(send_buffer));
    // initializing ICMP header
    icmp = (struct icmp *) send_buffer;
    icmp->icmp_type = ICMP_ECHO;
    icmp->icmp_code = 0;
    icmp->icmp_id = htons(0x13); // process identified by PID
    icmp->icmp_seq = htons(0); // sequential number
    data_len = snprintf(((char *) send_buffer + ICMP_HEADER_LEN),
                        sizeof(send_buffer) - ICMP_HEADER_LEN, "%d",
                        htonl(879296770));
    if (data_len < 1)
        syserr("snprinf");
    icmp_len = data_len + ICMP_HEADER_LEN; // packet is filled with 0
    icmp->icmp_cksum = 0; // checksum computed over whole ICMP package
    icmp->icmp_cksum = in_cksum((unsigned short *) icmp, icmp_len);
    *timeBefore = countMicroseconds();
    len = sendto(sock, (void *) icmp, icmp_len, 0,
                 (struct sockaddr *) &send_addr, (socklen_t) sizeof(send_addr));
    if (icmp_len != (ssize_t) len)
        syserr("partial / failed write");
}

int receive_ping_reply(int sock, int *timeBefore, int *timeAfter) {
    struct sockaddr_in rcv_addr;
    socklen_t rcv_addr_len;
    struct ip *ip;
    struct icmp *icmp;
    char rcv_buffer[BSIZE];
    ssize_t ip_header_len = 0;
    ssize_t icmp_len = 0;
    ssize_t len;
    memset(rcv_buffer, 0, sizeof(rcv_buffer));
    rcv_addr_len = (socklen_t) sizeof(rcv_addr);
    len = recvfrom(sock, (void *) rcv_buffer, sizeof(rcv_buffer), 0,
                   (struct sockaddr *) &rcv_addr, &rcv_addr_len);
    if (len < 0) {
        *timeBefore = 0;
        *timeAfter = 10000000;
        return 1;
    }
    // recvfrom returns whole packet (with IP header)
    ip = (struct ip *) rcv_buffer;
    ip_header_len = ip->ip_hl << 2; // IP header len is in 4-byte words
    icmp = (struct icmp *) (rcv_buffer + ip_header_len); // ICMP header follows IP
    icmp_len = len - ip_header_len;
    if (icmp_len < ICMP_HEADER_LEN)
        fatal("icmp header len (%d) < ICMP_HEADER_LEN", icmp_len);
    if (icmp->icmp_type != ICMP_ECHOREPLY) {
        return 0;
    }
    if (ntohs(icmp->icmp_id) != 0x13)
        fatal("reply with id %d different from my pid %d", ntohs(icmp->icmp_id),
              getpid());
    *timeAfter = countMicroseconds();
    return 1;
}

static void singleCountDelay(int sock, struct UserConfiguration *configuration,
                             int whichCount) {
    int timeBefore = 0, timeAfter = 0, delay = 0;
    send_ping_request(sock, configuration->DelayCounterHostname,
                      &timeBefore, &timeAfter);
    while (!receive_ping_reply(sock, &timeBefore, &timeAfter));
    if (timeAfter - timeBefore >= 10000000) {
        delay = 10000000;
    } else {
        delay = timeAfter - timeBefore;
    }
    DelayCounterInsertDelayStatistic(configuration, 3, delay, whichCount
    );
}

void serviceICMPClient(struct UserConfiguration configuration) {
    int whichCount = 0;
    while (DelayCounterIsHostExistsInList(&configuration) == true) {
        int sock = 0;
        sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
        struct timeval timeout = {10, 0};
        setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout, sizeof(struct timeval));
        if (sock < 0)
            syserr("socket");
        singleCountDelay(sock, &configuration, whichCount);
        whichCount++;
        sleepSeconds(configuration.measureDelayTimeIntervals);
        if (close(sock) == -1)
            syserr("close");
    }
}
