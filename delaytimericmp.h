/*
 * delaytimericmp.h
 *
 *  Created on: 23 maj 2015
 *      Author: domandinho
 */

#ifndef DELAYTIMERICMP_H_
#define DELAYTIMERICMP_H_

#include "config.h"

void serviceICMPClient(struct UserConfiguration configuration);

#endif /* DELAYTIMERICMP_H_ */
